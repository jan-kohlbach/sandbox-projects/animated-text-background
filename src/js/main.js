const headline = document.querySelector('h1');

window.addEventListener('mousemove', (e) => {
  const X_DECREASE = 50;
  const Y_DECREASE = 80;

  let x = (e.x / window.innerWidth) * X_DECREASE;
  let y = (e.y / window.innerHeight) * Y_DECREASE;

  x = 100 - ((100 - X_DECREASE) / 2) - Math.floor(x);
  y = 100 - ((100 - Y_DECREASE) / 2) - Math.floor(y);

  headline.style.backgroundPosition = `${x}% ${y}%`;
});
